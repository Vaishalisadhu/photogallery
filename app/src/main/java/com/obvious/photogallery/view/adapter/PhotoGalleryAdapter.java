package com.obvious.photogallery.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.obvious.photogallery.R;
import com.obvious.photogallery.view.model.ImageDataModel;
import com.obvious.photogallery.view.view.FullScreen;

import java.util.List;
import java.util.PriorityQueue;

public class PhotoGalleryAdapter extends RecyclerView.Adapter<PhotoGalleryAdapter.ViewHolder> {

    private List<ImageDataModel> imageDataModels;
    private Context context;
    private String title, description;


    public PhotoGalleryAdapter(List<ImageDataModel> imageDataModels, Context context) {

        this.imageDataModels = imageDataModels;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.recycler_layout, parent, false);
        return new PhotoGalleryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        ImageDataModel imageDataModel = imageDataModels.get(position);

        if (imageDataModel != null) {

            if (imageDataModel.getImageUrl() != null && !TextUtils.isEmpty(imageDataModel.getImageUrl())) {

                Glide.with(context).load(imageDataModel.getImageUrl())
                        .into(holder.selectedImage);
            }

            if (imageDataModel.getTitle() != null && !TextUtils.isEmpty(imageDataModel.getTitle())) {

                title = imageDataModel.getTitle();
            }
            if (imageDataModel.getDiscription() != null && !TextUtils.isEmpty(imageDataModel.getDiscription())) {
                description = imageDataModel.getDiscription();
            }
            holder.selectedImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, FullScreen.class);
                    i.putExtra("position", position);
                    i.putExtra("title", title);
                    i.putExtra("description", description);
                    context.startActivity(i);
                }
            });


        }


    }

    @Override
    public int getItemCount() {
        return imageDataModels.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        private ImageView selectedImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            selectedImage = itemView.findViewById(R.id.selectedImage);


        }

    }
}


