package com.obvious.photogallery.view.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.obvious.photogallery.R;
import com.obvious.photogallery.view.adapter.PhotoGalleryAdapter;
import com.obvious.photogallery.view.model.ImageDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvPhotoGallery;
    private GridLayoutManager gridLayoutManager;
    private List<ImageDataModel> imageData;
    private PhotoGalleryAdapter photoGalleryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        setData();
    }


    private void init() {

        // get the reference of RecyclerView
        rvPhotoGallery = findViewById(R.id.rvPhotoGallery);
        // set a GridLayoutManager with 3 number of columns
        gridLayoutManager = new GridLayoutManager(this, 2);
        // set Horizontal Orientation
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        // set LayoutManager to RecyclerView
        rvPhotoGallery.setLayoutManager(gridLayoutManager);
        rvPhotoGallery.setHasFixedSize(true);


        imageData = new ArrayList<>();
    }


    private void setData() {

        try {
            // get JSONarray from JSON file
            JSONArray jsonArray = new JSONArray(loadJSONFromAsset());

            // implement for loop for getting users list data
            for (int i = 0; i < jsonArray.length(); i++) {

                // create a JSONObject for fetching single user data
                JSONObject userDetail = jsonArray.getJSONObject(i);

                // fetch imgUrl and title and store it in arraylist
                ImageDataModel imageDataModel = new ImageDataModel();
                imageDataModel.setImageUrl(userDetail.getString("url"));
                imageDataModel.setTitle(userDetail.getString("title"));
                imageDataModel.setDiscription(userDetail.getString("explanation"));
                imageData.add(imageDataModel);

            }


            if (imageData != null && imageData.size() > 0) {

                photoGalleryAdapter = new PhotoGalleryAdapter(imageData, this);
                rvPhotoGallery.setAdapter(photoGalleryAdapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
